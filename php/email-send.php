<?php
$mail_to = 'hugogomes@modular-studio.com';
$subject = 'Mensagem de contacto telefónico.';

$nome = $_POST['nomeComp'];
$tel = $_POST['telefInput'];
$email = $_POST['emailInput'];
$msg = $_POST['descrConsult'];
$option = $_POST['doutor'];

switch($option)
{
	case 0:
		$dr = "Indiferente";
		break;
	case 1:
		$dr = "Dr. Sandra Santos";
		break;
	case 2:
		$dr = "Dra. Ricardo Coelho";
		break;
	case 3:
		$dr = "Dra. Susana Abreu";
		break;
	case 4:
		$dr = "Dra. Sara Brasileiro";
		break;
	case 5:
		$dr = "Dra. Venusa Bezerra";
		break;
}

$option = $_POST['altura'];

switch($option)
{
	case 0:
		$altura = "Indiferente";
		break;
	case 1:
		$altura = "Manhã";
		break;
	case 2:
		$altura = "Tarde";
		break;
}


$body_message = "Novo pedido de marcação:\nNome: ".$nome." | Telefone: ".$tel." | E-mail: ".$email." | Doutor: ".$dr." | Altura do dia: ".$altura."\n".$msg;

$mail_status = mail($mail_to, $subject, $body_message);

if($mail_status) {
       // Sucesso
       echo "O seu pedido foi registado, aguarde pelo nosso contacto.";
}else{
        // Erro
        echo "Erro ao enviar pedido.";
}
header("Location: ../sucesso.html")
?>